import React from 'react';
import './sass/core.scss';
import Title from './Components/Title';
import Form from './Components/Form';
import Weather from './Components/Weather';

const App = () => (
    <div className="container auto flex flex-wrap box-shadow">
        <div className="col-6 left-panel flex justify-center align-center">
            <Title/>
        </div>
        <div className="col-6 padt80 padb80 padr80 padl80 bg--gray">
            <Form/>
        </div>
    </div>
);


export default () => (
    <App/>
)