import React from 'react';
import {compose} from 'recompose';

const Title = () => (
    <div>
        <h1 className='small txt-center col-white'>
            Check weather in your city...
        </h1>
    </div>
);

export default compose()(Title);