import React from 'react';
import {compose, withHandlers, withState} from 'recompose';

const Weather = ({forecast}) => (
    <div className="mt50 col-white">
        <p>City: {forecast.name}</p>
        <p>Country: {forecast.sys.country}</p>
        <p>Temperature: {forecast.main.temp} &deg;C</p>
        <p>Pressure: {forecast.main.pressure} hPa</p>
        <p>Humidity: {forecast.main.humidity} %</p>
        <p>Wind: {forecast.wind.speed} km/h</p>
        {console.log(forecast)}
    </div>
);

export default compose()(Weather);
