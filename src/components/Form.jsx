import React from 'react';
import {compose, withHandlers, withState} from 'recompose';
import Weather from "./Weather";

const Form = ({getWeather, onChange, forecast}) => (
    <div>
        <label htmlFor="city" className="row">
            <input type="text" id='city' placeholder='City...' onChange={onChange('city')}/>
        </label>
        <br/>
        <label htmlFor="country" className="row">
            <input type="text" id='country' placeholder='Country...' onChange={onChange('country')}/>
        </label>
        <div className="row mt35">
            <button className="btn btn--orenge padt15 padb15 padl15 padr15" onClick={getWeather}>Check weather</button>
        </div>
        {
            forecast && <Weather forecast={forecast}/>
        }
    </div>
);

export default compose(
    withState('data', 'fillData', {
        city: '',
        country: '',
        api_key: '83b4508b7609e6286845436f81394b7c'
    }),
    withState('forecast', 'fillForecast', null),
    withHandlers({
        getWeather: ({data, fillForecast}) => async () => {

            // e.preventDefault();
            const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${data.city},${data.country}&appid=${data.api_key}&units=metric`);
            let get = await api_call.json();

            fillForecast(get)

        },
        onChange: props => inputName => e => {
            props.fillData(Object.assign({}, props.data, {[inputName]: e.target.value}))
        }

    })
)(Form);